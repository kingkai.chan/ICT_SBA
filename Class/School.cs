﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace ICT_SBA
{
    class School
    {
        public int id{ get; protected set; }
        public List<Student> studentList { get; protected set; }

        public School(int id) { this.id = id; studentList = new List<Student>(); }
    }
}
