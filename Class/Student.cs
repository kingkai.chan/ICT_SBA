﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace ICT_SBA
{
    class Student
    {
        public string name { get; protected set; }
        public bool isSeed { get; protected set; }
        public School school { get; protected set; }

        public Student(string name, bool isSeed){ this.name = name;this.isSeed = isSeed; }
    }
}
