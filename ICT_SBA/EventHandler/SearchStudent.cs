﻿using System.Windows.Controls;
using System.Windows.Data;

namespace ICT_SBA
{
    public partial class MainWindow
    {
        private void SearchStudent(object sender, TextChangedEventArgs e)
        {
            //Get the text in search field
            var filterText = StudentSearchTextBox.Text.ToLower();
            //Get the data grid displaying school entries
            var cv = CollectionViewSource.GetDefaultView(StudentDataGrid.ItemsSource);
            cv.Filter = o =>
            {
                var student = o as Student;
                return student.name.ToLower().Contains(filterText); //Filter matched entries
            };
        }
    }
}