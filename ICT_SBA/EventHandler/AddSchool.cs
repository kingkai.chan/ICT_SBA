﻿using System.Windows;

namespace ICT_SBA
{
    public partial class MainWindow
    {
        private void AddSchool(object sender, RoutedEventArgs e)
        {
            var schoolName = SchoolNameTextBox.Text; //Get user inputted school name
            if (CheckSchool(schoolName)) //Check for validity
            {
                schoolList.Add(new School(schoolName)); //Add to school list
                MessageBox.Show("School " + schoolName + " Added.", "School Added!", MessageBoxButton.OK,
                    MessageBoxImage.Information);
                //Prompt user for successful data input
                Update(); //Update the state of the program
            }
            else
            {
                MessageBox.Show("Invalid Input!", "Invalid Input!", MessageBoxButton.OK, MessageBoxImage.Error);
                //Prompt user for rejected data input
            }
        }
    }
}