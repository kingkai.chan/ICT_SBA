﻿using System.Linq;
using System.Windows;

namespace ICT_SBA
{
    public partial class MainWindow
    {
        private void DeleteSchool(object sender, RoutedEventArgs e)
        {
            //Get selected school entries
            var selected = SchoolDataGrid.SelectedItems.Cast<School>().ToList();
            if (selected.Count == 0) //Handle no entry selected
            {
                MessageBox.Show("Please select at least one entry!", "No Entry Selected", MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return;
            }

            foreach (var i in selected) schoolList.Remove(i); //Delete entries
            MessageBox.Show(selected.Count + " school(s) removed.", "School(s) removed.", MessageBoxButton.OK,
                MessageBoxImage.Information);
            Update(); //Update current state of the program
        }
    }
}