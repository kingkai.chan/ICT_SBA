﻿using System.ComponentModel;
using System.Windows;

namespace ICT_SBA
{
    public partial class MainWindow
    {
        private void Unload(object sender, CancelEventArgs e)
        {
            if (MessageBox.Show("Are you sure to close the application? Data will be lost.", "Warning",
                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No) e.Cancel = true;
        }
    }
}