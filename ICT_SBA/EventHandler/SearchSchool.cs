﻿using System.Windows.Controls;
using System.Windows.Data;

namespace ICT_SBA
{
    public partial class MainWindow
    {
        private void SearchSchool(object sender, TextChangedEventArgs e)
        {
            //Get the text in search field
            var filterText = SchoolSearchTextBox.Text.ToLower();
            //Get the data grid displaying school entries
            var cv = CollectionViewSource.GetDefaultView(SchoolDataGrid.ItemsSource);
            cv.Filter = o =>
            {
                var school = o as School;
                return school.name.ToLower().Contains(filterText); //Filter matched entries
            };
        }
    }
}