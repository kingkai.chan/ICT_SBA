﻿using System.Windows;

namespace ICT_SBA
{
    public partial class MainWindow
    {
        private void DeleteStudent(object sender, RoutedEventArgs e)
        {
            //Get selected student entries
            var selected = StudentDataGrid.SelectedItems;
            //Get number of selected students
            var temp = selected.Count;
            if (selected.Count == 0) //Handle no entry selected
            {
                MessageBox.Show("Please select at least one entry!", "No Entry Selected", MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return;
            }

            foreach (var student in selected) //Delete entries
            foreach (var school in schoolList)
                school.studentList.Remove((Student) student);

            MessageBox.Show(temp + " student(s) removed.", "Student(s) removed.", MessageBoxButton.OK,
                MessageBoxImage.Information);
            Update(); //Update current state of the program
        }
    }
}