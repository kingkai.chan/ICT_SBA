﻿using System.Windows;

namespace ICT_SBA
{
    public partial class MainWindow
    {
        private void AddStudent(object sender, RoutedEventArgs e)
        {
            var studentName = StudentNameTextBox.Text; //Get user inputted student name
            var isSeed = (bool) IsSeedCheckBox.IsChecked; //Get the seed player status
            var belong = (School) SchoolListComboBox.SelectedItem; //Get the selected school
            if (CheckStudent(studentName, isSeed, belong, 0)) //Check validity of student
            {
                var student = new Student(studentName, isSeed, belong.name); //Create new student entry
                foreach (var school in schoolList) //Add to studentList in school
                    if (school.name == belong.name)
                    {
                        if (isSeed) seedCount++;
                        school.studentList.Add(student);
                        break;
                    }

                MessageBox.Show("Student " + studentName + " Added.", "Student Added!", MessageBoxButton.OK,
                    MessageBoxImage.Information);
                Update(); //Update the state of the program
            }
            else
            {
                MessageBox.Show("Invalid Input!", "Invalid Input!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}