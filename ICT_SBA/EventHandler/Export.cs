﻿using System.IO;
using System.Windows;
using System.Windows.Forms;
using MessageBox = System.Windows.MessageBox;

namespace ICT_SBA
{
    public partial class MainWindow
    {
        private void Export(object sender, RoutedEventArgs e) //The export function
        {
            var saveFileDialog = new SaveFileDialog
            {
                Filter = "Comma-separated values Files|*.csv",
                Title = "Select a .csv"
            }; //Initialize properties of the File Dialog
            saveFileDialog.ShowDialog();
            var pathToFile = "";
            if (saveFileDialog.FileName != "") pathToFile = saveFileDialog.FileName;
            else return;
            //Handle cancel export
            var file = new StreamWriter(pathToFile, false); //Initialize stream writer for file export
            foreach (var student in students)
                file.WriteLine("{0},{1},{2}", student.name, student.isSeed ? "true" : "false", student.school);
            //Write data to file
            file.Close();
            MessageBox.Show("Export Success!", "Exported", MessageBoxButton.OK, MessageBoxImage.Information);
            //Prompt user for finishing data export
        }
    }
}