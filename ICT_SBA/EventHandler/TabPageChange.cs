﻿using System.Windows.Controls;

namespace ICT_SBA
{
    public partial class MainWindow
    {
        private void TabPageChange(object sender, SelectionChangedEventArgs e)
        {
            SchoolSearchTextBox.Clear();
            StudentSearchTextBox.Clear();
        }
    }
}