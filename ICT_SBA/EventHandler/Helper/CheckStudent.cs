﻿using System.Text.RegularExpressions;

namespace ICT_SBA
{
    public partial class MainWindow
    {
        public bool CheckStudent(string studentName, bool isSeed, School school, int mode)
        {
            //Check Rule 1, 2 and 3
            if ((mode == 1 ? tempSeedCount : seedCount) == 4 && isSeed || school == null ||
                !Regex.IsMatch(studentName, "^[a-zA-Z1-9\\s]+$"))
                return false;
            foreach (var i in mode == 1 ? tempSchoolList : schoolList)
                if (i.name == school.name)
                {
                    //Check Rule 4
                    if (school.studentList.Count == 2) return false;
                    break;
                }

            return true;
        }
    }
}