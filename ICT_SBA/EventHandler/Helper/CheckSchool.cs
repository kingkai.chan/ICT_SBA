﻿using System.Linq;
using System.Text.RegularExpressions;

namespace ICT_SBA
{
    public partial class MainWindow
    {
        public bool CheckSchool(string schoolName)
        {
            //Check Rule 2 and 3
            if (!Regex.IsMatch(schoolName, "^[a-zA-Z\\s]+$") || schoolName.Length == 0) return false;
            //Check Rule 1
            return schoolList.ToList().FindIndex(school => school.name.Equals(schoolName)) < 0;
        }
    }
}