﻿namespace ICT_SBA
{
    public partial class MainWindow
    {
        public int NextPow(int x)
        {
            if ((x & (x - 1)) == 0) return x;
            while ((x & (x - 1)) > 0) x &= x - 1;
            return x << 1;
        }
    }
}