﻿using System.Diagnostics;
using System.IO;
using System.Web.UI;
using System.Windows.Forms;

namespace ICT_SBA
{
    public partial class MainWindow
    {
        public void WriteHtml(Student[] match, int studentCount)
        {
            var saveFileDialog = new SaveFileDialog
            {
                Filter = "HTML Files|*.html",
                FileName = "index.html"
            }; //Initialize properties of the File Dialog
            saveFileDialog.ShowDialog();
            var pathToFile = ""; //Initialize the path of HTML to be saved
            if (saveFileDialog.FileName != "") pathToFile = saveFileDialog.FileName;
            else return; //Handle cancel generation
            var file = new StreamWriter(pathToFile, false);
            var html = "";
            var stringWriter = new StringWriter();
            var css = //CSS of used in the HTML
                "main{display:flex;flex-direction:row}.col{display:flex;flex-direction:column;justify-content:center;width:auto;min-width:200px;list-style:none;padding:0}.col .space{flex-grow:1}.col .space:first-child,.col .space:last-child{flex-grow:.5}.col .graph-space{flex-grow:1}body{font-family:sans-serif;font-size:small;padding:10px;white-space:nowrap}li.graph{padding-left:20px}li.graph span{float:right;margin-right:5px}li.graph-top{border-bottom:2px solid #aaa}li.graph-space{border-right:2px solid #aaa;min-height:40px}li.graph-bottom{border-top:2px solid #aaa}";
            using (var writer = new HtmlTextWriter(stringWriter))
            {
                writer.Write(
                    @"<html>
                        <head>
                            <title>Competition Graph</title>
                        <style>{0}</style>
                        </head>
                        <main class=""competition"">
                            <ul class=""col col-1"">
                                <li class=""space"">&nbsp;</li>",
                    css);
                var cur = 2;
                for (var i = 0; i < studentCount; i += 2)
                {
                    var name1 = match[i].name + "(" + match[i].school + ")";
                    var name2 = match[i + 1].name + "(" + match[i + 1].school + ")";
                    if (match[i].isSeed) name1 = "<b>" + name1 + "</b>";
                    if (match[i + 1].isSeed) name2 = "<b>" + name2 + "</b>";
                    writer.Write(
                        @"
                                <li class=""graph graph-top"">{0}</li>
                                <li class=""graph graph-space"">&nbsp;</li>
                                <li class=""graph graph-bottom"">{1}</li>
                                <li class=""space"">&nbsp;</li>",
                        name1, name2);
                }

                writer.Write(@"
                            </ul>"
                );
                studentCount /= 4;
                while (studentCount >= 1)
                {
                    writer.Write(@"
                            <ul class=""col col-{0}"">
                                <li class=""space"">&nbsp;</li>", cur);
                    for (var i = 0; i < studentCount; i++)
                        writer.Write(
                            @"
                                <li class=""graph graph-top""></li>
                                <li class=""graph graph-space"">&nbsp;</li>
                                <li class=""graph graph-bottom""></li><li class=""space"">&nbsp;</li>
                                ");
                    writer.Write(@"
                            </ul>");
                    studentCount /= 2;
                    cur++;
                }

                writer.Write(@"
                        </main>
                    </html>");
                //Write to the file
                file.WriteLine(stringWriter.ToString());
                file.Close();
                html = stringWriter.ToString();
            }

            //Display the HTML in browser
            Process.Start(pathToFile);
        }
    }
}