﻿using System.Collections.Generic;
using System.Linq;

namespace ICT_SBA
{
    public partial class MainWindow
    {
        public bool ImportAddStudent(List<string> fields)
        {
            //Check Rule 1
            if (fields.Count < 3) return false;
            var studentName = fields[0]; //Parse student name
            // ReSharper disable once ArrangeRedundantParentheses
            var isSeed = (fields[1] == "true"); //Parse seed status
            School belong = null;
            foreach (var school in tempSchoolList) //Parse school
                if (school.name == fields[2])
                {
                    belong = school;
                    break;
                }

            if (belong == null) belong = new School(fields[2]);
            if (!CheckStudent(studentName, isSeed, belong, 1)) return false; //Check validity with mode 1
            var student = new Student(studentName, isSeed, belong.name);
            var ok = false;
            foreach (var school in tempSchoolList)
                if (school.name == belong.name) //Check if the school exist and add entry to it if so
                {
                    if (isSeed) tempSeedCount++;
                    school.studentList.Add(student);
                    ok = true;
                }

            if (ok) return true;
            //Create a new school and add the entry to it
            if (isSeed) tempSeedCount++;
            tempSchoolList.Add(belong);
            tempSchoolList.Last().studentList.Add(student);
            return true;
        }
    }
}