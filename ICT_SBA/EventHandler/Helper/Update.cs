﻿namespace ICT_SBA
{
    public partial class MainWindow
    {
        public void Update()
        {
            //Reset the variables
            seedCount = 0;
            students.Clear();
            IsSeedCheckBox.IsChecked = false;
            //Refresh the list of student
            foreach (var school in schoolList)
            foreach (var student in school.studentList)
                if (student.isSeed)
                {
                    seedCount++;
                    students.Insert(0, student);
                }
                else
                {
                    students.Add(student);
                }

            //Refresh the GUI
            SchoolListComboBox.Items.Refresh();
            SchoolDataGrid.Items.Refresh();
            StudentDataGrid.ItemsSource = students;
            StudentDataGrid.Items.Refresh();
        }
    }
}