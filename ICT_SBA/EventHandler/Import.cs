﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows;
using System.Windows.Forms;
using MessageBox = System.Windows.MessageBox;

namespace ICT_SBA
{
    public partial class MainWindow
    {
        private readonly ObservableCollection<School> tempSchoolList = new ObservableCollection<School>();

        //Initialize a temporary list for data handling
        private int tempSeedCount;

        //Variable for storing the new seed player count
        private void Import(object sender, RoutedEventArgs e) //The import function
        {
            var openFileDialog = new OpenFileDialog
            {
                Filter = "Comma-separated values Files|*.csv",
                Title = "Select a .csv",
                Multiselect = false
            }; //Initialize the property of the File Dialog
            var pathToFile = ""; //Initialize the path of file to be imported
            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                pathToFile = openFileDialog.FileName;
            else return;
            //Handle canceling import
            var messageBoxResult = MessageBox.Show(
                "Are you sure? your current data will be lost.", "Import Confirmation.",
                MessageBoxButton.YesNo); //Prompt user the data will be lost
            if (File.Exists(pathToFile) && messageBoxResult == MessageBoxResult.Yes)
            {
                var lines = File.ReadAllLines(pathToFile); //Read lines in the file
                tempSchoolList.Clear();
                tempSeedCount = 0;

                foreach (var line in lines)
                    if (!ImportAddStudent(new List<string>(line.Split(',')))) //Parse the line and check for validity
                    {
                        MessageBox.Show("Invalid csv!", "Invalid csv!", MessageBoxButton.OK,
                            MessageBoxImage.Error); //Invalid data handling
                        return;
                    }
            }
            else
            {
                return;
            }

            schoolList.Clear();
            students.Clear();
            foreach (var school in tempSchoolList) schoolList.Add(school); //Replace the existing data
            Update(); //Update the state of the program
        }
    }
}