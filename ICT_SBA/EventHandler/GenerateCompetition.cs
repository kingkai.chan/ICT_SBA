﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace ICT_SBA
{
    public partial class MainWindow
    {
        private readonly Student bye = new Student("Bye", false, "N/A");

        private List<Student> BinaryGraphGen(List<Student> x, int byecount)
        {
            if (x.Count + byecount == 2) //Base case
            {
                x.AddRange(Enumerable.Repeat(bye, byecount));
                return x;
            }

            //Generate Group 1
            var list1 = BinaryGraphGen(x.Where((student, index) => index % 2 == 1).ToList(),
                byecount / 2 + byecount % 2);
            //Generate Group 2
            var list2 = BinaryGraphGen(x.Where((student, index) => index % 2 == 0).ToList(), byecount / 2);
            //Append Group 2 to Group 1
            list1.AddRange(list2);
            //Return the optimal solution
            return list1;
        }

        private void GenerateCompetition(object sender, RoutedEventArgs e)
        {
            var genlist = students.ToList();
            var studentCount = students.Count;
            if (studentCount <= 1) //Handle exceptional case
            {
                MessageBox.Show("There are not enough student!", "Not enough student", MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return;
            }

            //Generate the match sequence
            genlist = BinaryGraphGen(genlist, NextPow(studentCount) - studentCount);
            //Generate the graphical representation
            WriteHtml(genlist.ToArray(), NextPow(studentCount));
        }
    }
}