﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace ICT_SBA
{
    public class School : INotifyPropertyChanged
    {
        private string _name;
        private ObservableCollection<Student> _studentList;

        public School(string name) //Constructor of School class
        {
            this.name = name;
            studentList = new ObservableCollection<Student>();
        }

        public string name //The name of school
        {
            get => _name;
            set => SetField(ref _name, value, "name");
        }

        public ObservableCollection<Student> studentList //The list of students of the school
        {
            get => _studentList;
            set => SetField(ref _studentList, value, "studentList");
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected bool SetField<T>(ref T field, T value, string propertyName)
        {
            if (EqualityComparer<T>.Default.Equals(field, value)) return false;
            field = value;
            OnPropertyChanged(propertyName);
            return true;
        }
    }
}