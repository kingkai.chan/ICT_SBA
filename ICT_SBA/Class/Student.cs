﻿using System.Collections.Generic;
using System.ComponentModel;

namespace ICT_SBA
{
    public class Student : INotifyPropertyChanged
    {
        private bool _isSeed;
        private string _name;
        private string _school;

        public Student(string name, bool isSeed, string school) //Constructor of Student class
        {
            this.name = name;
            this.isSeed = isSeed;
            this.school = school;
        }

        public string name //Name of the student
        {
            get => _name;
            set => SetField(ref _name, value, "name");
        }

        public string school //Name of school the student belongs to
        {
            get => _school;
            set => SetField(ref _school, value, "school");
        }

        public bool isSeed //Whether the student is a seed player
        {
            get => _isSeed;
            set => SetField(ref _isSeed, value, "isSeed");
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected bool SetField<T>(ref T field, T value, string propertyName)
        {
            if (EqualityComparer<T>.Default.Equals(field, value)) return false;
            field = value;
            OnPropertyChanged(propertyName);
            return true;
        }
    }
}