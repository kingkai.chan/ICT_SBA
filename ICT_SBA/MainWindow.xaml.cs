﻿using System.Collections.ObjectModel;
using System.Windows;

namespace ICT_SBA
{
    public partial class MainWindow : Window
    {
        public ObservableCollection<School> schoolList; //List of school
        public int seedCount; //Count of number of seed players
        public ObservableCollection<Student> students; //List of student

        public MainWindow()
        {
            InitializeComponent(); //Default Implementation of GUI Initialization
            schoolList = new ObservableCollection<School>(); //Initialization of list of school
            students = new ObservableCollection<Student>(); //Initialization of list of student
            AddSchoolButton.Click += AddSchool; //Bind Event Handler for Manual Add School Button
            AddStudentButton.Click += AddStudent; //Bind Event Handler for Manual Add Student Button
            DeleteSchoolButton.Click += DeleteSchool; //Bind Event Handler for Delete School Button
            DeleteStudentButton.Click += DeleteStudent; //Bind Event Handler for Delete Student Button
            SchoolSearchTextBox.TextChanged += SearchSchool; //Bind Event Handler for Search School Function
            StudentSearchTextBox.TextChanged += SearchStudent; //Bind Event Handler for Search Student Function
            TabPageController.SelectionChanged += TabPageChange; //Bind Event Handler for Handling TabPage Change
            GenerateGraphButton.Click += GenerateCompetition; //Bind Event Handler for Generating Tournament
            ImportButton.Click += Import; //Bind Event Handler for Import Button
            ExportButton.Click += Export; //Bind Event Handler for Export Button
            SchoolListComboBox.ItemsSource = schoolList; //Bind data source of Combo Box for school selection
            SchoolListComboBox.DisplayMemberPath = "name"; //Configure display member of Combo Box for school selection
            SchoolDataGrid.ItemsSource = schoolList; //Bind data source of school list display
            StudentDataGrid.ItemsSource = students; //Bind data source of student list display
            SchoolDataGrid.IsReadOnly = true; //Prevent undesired modification
            StudentDataGrid.IsReadOnly = true; //Prevent undesired modification
            Closing += Unload; //Prompt User to save the data before leaving
        }
    }
}